package fusion.page.common;

import static org.junit.Assert.fail;

import java.util.Iterator;
import java.util.Set;

import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageParent {
	
	/**
	 * Opens the login Page.
	 *
	 */
	public void openLoginPage() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        String baseUrl = testParams.getProperty("base_url");
        webDriver.get("https://login.fusionwebclinic.com/login");

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
}
