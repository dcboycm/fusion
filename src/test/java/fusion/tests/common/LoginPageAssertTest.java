/*----------------------------------------------------------------------
 * Filename: LoginPageAssertTest.java
 *
 *
 */

package fusion.tests.common;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import fusion.selenium.BaseSeleniumTest;
import fusion.page.common.LoginPage;

public class LoginPageAssertTest extends BaseSeleniumTest {
	
    private LoginPage loginPage = new LoginPage();
    
    /**
     * Assert Login Page Texts Present
     *
     *
     *
     */
    @Test
    public void loginPageElementsTests() {

        String testId = "Login Page Elements Tests 1";

        System.out.println("Entered " + testId);

        // Simple Selenium wait so that we know the page has loaded what we want it to before continuing on
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
        selenium.isElementPresent("logo");
        selenium.isTextPresent("Email:");
        selenium.isTextPresent("Password:");
        selenium.isTextPresent("Login");
        selenium.isTextPresent("Forgot Your Password?");
        selenium.isTextPresent("By using this service, or permitting any other person or other entity to use this service on your behalf, you acknowledge that you have read these");
        selenium.isTextPresent("and that you accept");
        selenium.isTextPresent("and will be bound by the terms thereof.");
        Boolean emailPresent = webDriver.findElement(By.name("email")).isDisplayed();
        assertEquals(true, emailPresent);
        Boolean passwordPresent = webDriver.findElement(By.name("password")).isDisplayed();
        assertEquals(true, passwordPresent);

    }
}
