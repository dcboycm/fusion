/*----------------------------------------------------------------------
 * Filename: SmokeTest.java
 *
 *
 *
 * How to add a new test class to the suite...
 *   1. Add an import statement for the new test class.
 *   2. Add an entry to the array of test classes.
 *
 */

package fusion.selenium.test_suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import fusion.selenium.BaseSeleniumTest;
import fusion.testing.annotation.type.IntegrationTest;
import fusion.tests.common.LoginPageAssertTest;

//Specify a runner class.
@RunWith(Suite.class)
//@formatter:off
//Specify an array of test classes.
@Suite.SuiteClasses({
	LoginPageAssertTest.class,
	
}

)
//@formatter:on
@Category(IntegrationTest.class)

public class SmokeTest extends BaseSeleniumTest{
    @BeforeClass

    public static void suiteStart() throws Exception {
        setup();
//        login();
    }

    @AfterClass
    public static void suiteEnd() throws Exception {
        tearDown();
//         logout();
    }


}
